<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<div id="secondary">

	<div style="height:100px;background-color:#222;z-index: 100002;">

		<div id="pi-x" style="text-align:center;float:right;cursor:pointer;font-size:45px;line-height:45px;height:50px;padding:2px 15px 5px 15px;width:35px;overflow:hidden;color:#555;">&times;</div>

		<h1 class="sidebar-title">
			<a href="<?php bloginfo('url'); ?>" style="color:white; title="<?php bloginfo('name'); ?>">
				<?php bloginfo('name'); ?>
			</a>
		</h1>

	</div><!-- blog name -->

	<?php if ( has_nav_menu( 'secondary' ) ) : ?>
	<div style="z-index: 100001;">
		<nav role="navigation" class="navigation site-navigation secondary-navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'secondary' ) ); ?>
		</nav>
	</div>
	<?php endif; ?>

	<!-- scrolling area -->
	<div id="pill-holder" style="background-color:#222;height:100%;overflow-y:auto;border-top: 1px solid #000;clear:both;z-index:100000;">
		
		<?php
			$args = array(
				'orderby' => 'name',
				'parent' => 0
				);
			$categories = get_categories( $args );
			echo '<div id="cats" style="height:1300px;margin:15px 10px 20px 10px;">
			
				<style>
					
					#pill-holder::-webkit-scrollbar { 
						width:2px;  // Safari and Chrome
					}
					
					.sidepill { float:left;font-size:13px;
						padding:1px 5px;border-radius:3px;
						background-color:#5E5D54;margin:3px;
						box-shadow: 0px 0px 1px 0px rgba(0,0,0,1);
					}
					.sidepill:hover { 
						background-color:#4E4D44;
						box-shadow: inset 0px 4px 8px -7px rgba(0,0,0,1);
					}
			
				</style>';
				
			foreach ( $categories as $category ) {
				if ($category->cat_ID != 1) {
					echo '<div class="sidepill"><a style="color:white;" href="' . get_category_link( $category->term_id ) . '">' . $category->name . '</a></div>';
				}
			}
		?>
		
			<br style="clear:both;" />	
		</div><!-- cat pills -->
		
	
		<!--
		<div style="border-top: 1px solid #000;background-color:#222;z-index: 100002;height:100%;">
	
			<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
			<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
				<?php dynamic_sidebar( 'sidebar-1' ); ?>
			</div><!-- #primary-sidebar -->
			<?php endif; ?>
	
		</div>
		-->
	
	</div><!-- scrolling area -->	
	
</div><!-- #secondary -->
