# Minimal Mobile WordPress Theme

Very minimal WordPress theme. Derived from the official 2014 theme, but vastly simplified. Custom hamburger menu with tags. It's not designed for comments (yet) so you could add something like Disqus. Any issues, please email pj@pjbrunet.com

I could probably sell this theme on Envato, or distribute it on WordPress.org, but I haven't had time. If you want to work on some kind of collaboration, let me know. 

It's possible I left some personal junk in the code. If you find anything like that, let me know so I can remove it. 

## How did I come up with this design?

(31 minute video) https://pjbrunet.com/clean-your-wordpress-theme-to-increase-blog-monetization/

## What needs to be fixed?

On a 4K monitor the text is too small. I removed flowtype.js a while ago, intending to add back some responsive text using a CSS-only technique. But I never got around to fixing it. I'm thinking I could scale up the text size and the width of the blog at the same rate, such that the text would wrap the same (or nearly the same) at any resolution, without using a media query. 

I would also like to add a "dark mode" someday. White backgrounds are convenient when adding random images of various dimensions, especially transparent images. But these days I can't stand white backgrounds, too bright! Especially at night. However the problem is not that urgent, because I use a browser extension to make everything dark.

## Why do I like this theme?

- Looks good on mobile devices.
- It's good for ad monetization.
- I have a hunch it's good for SEO. Might work better with fewer categories, for higher-relevance.
- Good for reading. Lots of content above the fold.
- Not too many lines of code. It's easy to modify.
- The design is out of the way, not begging for attention. 

I tried some of the newer, official WordPress themes and many of them make you scroll too far before you get to see content, or they make the title too large, or they display the featured image. IMO the featured image is just for social sharing. 

