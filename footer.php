<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<div style="font-size:.8em;padding:0 3%;margin:auto;max-width:700px;">

	<!-- If the viewport is under 500 pixels, the ad breaks the layout! -->
	<style>		
		@media screen and (min-width: 0px) and (max-width: 350px) {
			#developer-content { display: none; }
            #developer-content2 { display: none; }
		}
	</style>

<?php 
	/* 
	
	<div id="developer-content" style="margin:2em 0;">
	<!-- Adsense was here. -->
	</div>

    */
?>
		
	<div style="text-decoration:none;color:#555;font-size:.9em;line-height:1.5em;clear:both;padding:4em 0 1em .25em;">
   		Content, CSS & Javascript &copy; 2014-<?php echo date("Y") ?> by PJ Brunet
	</div>
	
</div>

		</div><!-- #main -->

		<footer id="colophon" class="site-footer" role="contentinfo">
		<?php get_sidebar( 'footer' ); ?>

		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
	
</body>

<script>

	jQuery(function($) {
		
		var $navBody = $('#wpadminbar');
		var $navTop = 0;
		$navTop = $navBody.height();

		/*
		$('.jp-playlists').css({
				left: ($playList.offset().left + $playList.width()),
				top: $playList.offset().top + $(document).scrollTop()
		});
		*/

		$('#secondary').css({ 
			top: $navTop
		});
		
		
		$('#pi-burger').hover(

				function(){
						$('#pi-burger div').animate({'background-color': '#296e81'},0);
				},
				function(){
						$('#pi-burger div').animate({'background-color': '#444'},0);
				}

		);
		
		$('#pi-x').hover(

				function(){
						$(this).animate({'color': '#296e81'},0);
				},
				function(){
						$(this).animate({'color': '#555'},0);
				}

		);	
		
		/*
		$('.menu-item a').hover(

				function(){
						$(this).animate({'backgroundColor': '#335F80'},0);
				},
				function(){
						$(this).animate({'backgroundColor': '#2D3F4C'},500);
				}

		);
		*/

		<!-- all clicks/taps outside the slideout menu will close menu -->
		$("#primary").not("#secondary").click(function() {
			// Only works if #secondary menu is open
			if ( $("#primary").css('opacity') == '0.5' ) {
				$("#secondary").toggle('slide', { direction: "right" }, 300);
				$("#primary").fadeTo("fast", 1);
			}
		});
		
		<!-- Pi hamburger menu -->
		$("#pi-burger").click(function() {
			// $("#pi-burger").hide();
			$("#secondary").toggle('slide', { direction: "right" }, 300);
			$("#primary").fadeTo("fast", .5);
		});
		
		$("#menu-main li, #pi-x, #secondary h1, #secondary #cats a").click(function() { // Doesnt slide (yet) for second-level nav
			// $("#pi-burger").show();
			$("#secondary").toggle('slide', { direction: "right" }, 300);
			$("#primary").fadeTo("fast", 1);
		});

		$("body").show(); // fade in content after stuff is done jerking...
	
	});  // End of jQuery code

</script>

</html>
